/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { ApiApiResult } from '../models/api-api-result';
import { ApiTableFilterModel } from '../models/api-table-filter-model';
import { ApiTransactionRequest } from '../models/api-transaction-request';
import { ApiTransactionSearchResult } from '../models/api-transaction-search-result';

@Injectable({
  providedIn: 'root',
})
export class ApiTransactionsService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation balance
   */
  static readonly BalancePath = '/api/Transactions/balance';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `balance()` instead.
   *
   * This method doesn't expect any request body.
   */
  balance$Response(params?: {

  }): Observable<StrictHttpResponse<number>> {

    const rb = new RequestBuilder(this.rootUrl, ApiTransactionsService.BalancePath, 'get');
    if (params) {


    }
    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: parseFloat(String((r as HttpResponse<any>).body)) }) as StrictHttpResponse<number>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `balance$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  balance(params?: {

  }): Observable<number> {

    return this.balance$Response(params).pipe(
      map((r: StrictHttpResponse<number>) => r.body as number)
    );
  }

  /**
   * Path part for operation transactions
   */
  static readonly TransactionsPath = '/api/Transactions/search';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `transactions()` instead.
   *
   * This method sends `application/*+json` and handles request body of type `application/*+json`.
   */
  transactions$Response(params?: {
      body?: ApiTableFilterModel
  }): Observable<StrictHttpResponse<ApiTransactionSearchResult>> {

    const rb = new RequestBuilder(this.rootUrl, ApiTransactionsService.TransactionsPath, 'post');
    if (params) {


      rb.body(params.body, 'application/*+json');
    }
    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ApiTransactionSearchResult>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `transactions$Response()` instead.
   *
   * This method sends `application/*+json` and handles request body of type `application/*+json`.
   */
  transactions(params?: {
      body?: ApiTableFilterModel
  }): Observable<ApiTransactionSearchResult> {

    return this.transactions$Response(params).pipe(
      map((r: StrictHttpResponse<ApiTransactionSearchResult>) => r.body as ApiTransactionSearchResult)
    );
  }

  /**
   * Path part for operation createTransaction
   */
  static readonly CreateTransactionPath = '/api/Transactions/create';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createTransaction()` instead.
   *
   * This method sends `application/*+json` and handles request body of type `application/*+json`.
   */
  createTransaction$Response(params?: {
      body?: ApiTransactionRequest
  }): Observable<StrictHttpResponse<ApiApiResult>> {

    const rb = new RequestBuilder(this.rootUrl, ApiTransactionsService.CreateTransactionPath, 'post');
    if (params) {


      rb.body(params.body, 'application/*+json');
    }
    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ApiApiResult>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `createTransaction$Response()` instead.
   *
   * This method sends `application/*+json` and handles request body of type `application/*+json`.
   */
  createTransaction(params?: {
      body?: ApiTransactionRequest
  }): Observable<ApiApiResult> {

    return this.createTransaction$Response(params).pipe(
      map((r: StrictHttpResponse<ApiApiResult>) => r.body as ApiApiResult)
    );
  }

}
