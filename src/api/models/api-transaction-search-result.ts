/* tslint:disable */
import { ApiTransaction } from './api-transaction';
export interface ApiTransactionSearchResult {
  totalCount?: number;
  transactions?: null | Array<ApiTransaction>;
}
