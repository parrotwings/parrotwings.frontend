/* tslint:disable */
export interface ApiTransaction {
  amount?: number;
  correspondentId?: number;
  correspondentName?: null | string;
  createdDate?: string;
  id?: number;
  userId?: number;
}
