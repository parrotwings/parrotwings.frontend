/* tslint:disable */
export interface ApiTransactionRequest {
  amount?: number;
  userEmail?: null | string;
}
