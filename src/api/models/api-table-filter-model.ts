/* tslint:disable */
import { ApiTableFilterContext } from './api-table-filter-context';
import { ApiTableFilterSortMeta } from './api-table-filter-sort-meta';
export interface ApiTableFilterModel {
  filters?: null | { [key: string]: ApiTableFilterContext };
  first?: number;
  multiSortMeta?: null | Array<ApiTableFilterSortMeta>;
  rows?: number;
  sortField?: null | string;
  sortOrder?: number;
}
