/* tslint:disable */
export interface ApiTableFilterSortMeta {
  field?: null | string;
  order?: number;
}
