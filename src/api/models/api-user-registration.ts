/* tslint:disable */
export interface ApiUserRegistration {
  confirmPassword?: null | string;
  email?: null | string;
  password?: null | string;
}
