import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { DBkeys } from '../constants';
import { LocalStorageService } from '../services/local-storage.service';

@Injectable({
    providedIn: 'root',
})
export class AuthGuard implements CanActivate {
    constructor(private readonly router: Router, private readonly storageService: LocalStorageService) {}

    public canActivate(): boolean {
        const authToken: string | null = this.storageService.get(DBkeys.JWT_TOKEN);

        if (!!authToken) {
            return true;
        }

        this.router.navigate(['/account/login']);
        return false;
    }
}
