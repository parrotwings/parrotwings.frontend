import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { CoreRoutingModule } from './core-routing.module';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { BearerTokenInterceptor } from './interceptor/auth.interceptor';
import { ApiModule } from 'src/api';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import * as fromRoot from '../store';

@NgModule({
  declarations: [NotFoundComponent],
  imports: [
    CommonModule,
    CoreRoutingModule,
    StoreModule.forRoot(fromRoot.reducer),
    EffectsModule.forRoot([fromRoot.RootEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
    }),
  ],
  exports: [RouterModule],
  providers: [
    ApiModule,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BearerTokenInterceptor,
      multi: true,
    },

  ]
})
export class CoreModule { }
