import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DBkeys } from '../constants';
import { LocalStorageService } from '../services/local-storage.service';

@Injectable()
export class BearerTokenInterceptor implements HttpInterceptor {
    constructor(private readonly storageService: LocalStorageService) {}

    public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const bearerToken = this.storageService.get(DBkeys.JWT_TOKEN);

        if (!!bearerToken) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${bearerToken}`,
                },
            });
        }

        return next.handle(request);
    }
}
