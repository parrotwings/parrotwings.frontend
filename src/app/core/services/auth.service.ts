import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CustomEncoder } from '../utils/custom-encoder';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    constructor(private readonly httpClient: HttpClient) {}

    public login(userName: string, password: string): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded').set('Accept', 'application/json');

        const params: HttpParams = new HttpParams({ encoder: new CustomEncoder() })
            .set('grant_type', 'password')
            .set('scope', 'openid offline_access api')
            .set('username', userName)
            .set('password', password)
            .set('client_id', 'parrotwings_spa')
            .set('client_secret', 'not_used');

        return this.getToken(headers, params);
    }

    private getToken(headers: HttpHeaders, params: HttpParams): Observable<any> {
        return this.httpClient.post<any>(environment.apiUrl + '/connect/token', params.toString(), { headers });
    }
}