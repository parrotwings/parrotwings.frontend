import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class LocalStorageService {
    constructor() {}

    public get(key: string) {
        const result = localStorage.getItem(key);

        return JSON.parse(result);
    }

    public set(key: string, data: any): void {
        localStorage.setItem(key, JSON.stringify(data));
    }

    public delete(key: string): void {
        localStorage.removeItem(key);
    }
}
