import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './components/nav/nav.component';
import { MainLayoutComponent } from '../layout/main-layout/main-layout.component';
import { AnonLayoutComponent } from '../layout/anon-layout/anon-layout.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';

const SHARED_MODULES = [
  FormsModule,
  ReactiveFormsModule,
  InputTextModule,
  TableModule,
  MatInputModule,
  MatButtonModule,
];

@NgModule({
  declarations: [NavComponent, MainLayoutComponent, AnonLayoutComponent],
  imports: [CommonModule, RouterModule, SHARED_MODULES],
  exports: [SHARED_MODULES],
})
export class SharedModule {}
