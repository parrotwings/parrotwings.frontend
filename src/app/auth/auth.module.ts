import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationComponent } from './components/registration/registration.component';
import { LoginComponent } from './components/login/login.component';
import { RouterModule } from '@angular/router';
import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [RegistrationComponent, LoginComponent],
  imports: [CommonModule, AuthRoutingModule, SharedModule],
  exports: [RouterModule],
})
export class AuthModule {}
