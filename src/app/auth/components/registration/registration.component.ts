import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiAuthService, ApiUserRegistration } from 'src/api';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {
  public error: string;
  public isSubmited = false;
  public isSuccessfully = false;

  public registerForm = new FormGroup({
    email: new FormControl(null, Validators.required),
    password: new FormControl(null, Validators.required),
    confirmPassword: new FormControl(null, Validators.required),
  });

  constructor(private readonly authService: ApiAuthService) { }

  public ngOnInit(): void { }

  public onSubmit() {
    this.isSubmited = true;

    if (this.registerForm.invalid) {
      return;
    }

    this.error = null;

    var user: ApiUserRegistration = this.registerForm.value;

    this.authService.register({ body: user }).subscribe(result => {
      this.isSubmited = false;

      if (!result.successfully) {
        this.error = result.text;
        return;
      }
      this.isSuccessfully = true;
    })
  }
}
