import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AuthService } from 'src/app/core/services/auth.service';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { DBkeys } from 'src/app/core/constants';
import { Store } from '@ngrx/store';
import { LoginSuccess, IAppState } from 'src/app/store';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public error: string;
  public isSubmited = false;


  public loginForm = new FormGroup({
    email: new FormControl(null, Validators.required),
    password: new FormControl(null, [Validators.required]),
  });

  constructor(private readonly store: Store<IAppState>, private readonly authService: AuthService, private readonly storageService: LocalStorageService) { }

  public ngOnInit(): void { }

  public onSubmit(): void {
    this.isSubmited = true;
    if (this.loginForm.invalid) {
      return;
    }

    this.error = null;

    const { email, password } = this.loginForm.value;

    this.authService.login(email, password).subscribe(result => {
      this.isSubmited = false;
      if (result.access_token) {
        this.storageService.set(DBkeys.JWT_TOKEN, result.access_token);
        this.store.dispatch(LoginSuccess());
      }
    },
      error => {
        this.isSubmited = false;
        this.error = error?.error?.error_description;
      }
    );
  }
}
