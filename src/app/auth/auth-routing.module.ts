import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnonLayoutComponent } from '../layout/anon-layout/anon-layout.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';

const routes: Routes = [
    {
        path: '',
        component: AnonLayoutComponent,
        children: [
            {
                path: 'account',
                children: [
                    {
                        path: 'login',
                        component: LoginComponent,
                    },
                    {
                        path: 'registration',
                        component: RegistrationComponent,
                    },
                    {
                        path: '',
                        pathMatch: 'full',
                        redirectTo: 'login',
                    },
                ],
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AuthRoutingModule {}
