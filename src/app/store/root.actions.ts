import { createAction, props } from '@ngrx/store';
import { ApiTransactionRequest } from 'src/api';

export const LoginSuccess = createAction('[Auth] Login Success');
export const Logout = createAction('[Auth] Logout');

export const InitializeApp= createAction('[Init] initialize app');

export const GetCurrentUser = createAction('[User] Get current user');
export const GetCurrentUserSuccess = createAction('[User] Get current user success', props<{ user: any }>());

export const GetUserBalance = createAction('[User] Get user balance');
export const GetUserBalanceSuccess = createAction('[User] Get user balance success', props<{ balance: any }>());

export const GetTransactions = createAction('[Transactions] Get user transactions');
export const CreateTransaction = createAction('[Transactions] Create transaction', props<{ transaction: ApiTransactionRequest }>());

export const SomeError = createAction('[Error] Some error request', props<{ action: string; error: any }>());
