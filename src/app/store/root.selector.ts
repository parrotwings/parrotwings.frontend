import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromRoot from './root.reducer';

export const getRootFeatureState = createFeatureSelector<fromRoot.IRootState>('root');

export const getCurrentUser = createSelector(getRootFeatureState, (state) => state.user);
export const getUserBalance = createSelector(getRootFeatureState, (state) => state.balance);