import { DBkeys } from '../core/constants';
import { on, createReducer, ActionReducerMap, Action } from '@ngrx/store';
import { GetCurrentUserSuccess, Logout, GetUserBalanceSuccess } from './root.actions';

export interface IRootState {
    user: any;
    balance: number;
}

export interface IAppState {
    root: IRootState;
}

export const initialRootState: IRootState = {
    user: null,
    balance: 0
};

const rootReducer = createReducer(
    initialRootState,
    on(GetCurrentUserSuccess, (state: IRootState, { user }) => ({ ...state, user })),
    on(GetUserBalanceSuccess, (state: IRootState, { balance }) => ({ ...state, balance })),
    on(Logout, (state: IRootState) => ({  ...initialRootState })),
);

export function rootReducerOns(state: IRootState | undefined, action: Action) {
    return rootReducer(state, action);
}

export const reducer: ActionReducerMap<IAppState> = {
    root: rootReducerOns,
};

