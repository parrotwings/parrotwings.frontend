import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';

import { LoginSuccess, GetCurrentUserSuccess, GetCurrentUser, Logout, GetUserBalance, GetUserBalanceSuccess, SomeError, InitializeApp } from './root.actions';
import { switchMap, tap, map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { LocalStorageService } from '../core/services/local-storage.service';
import { DBkeys } from '../core/constants';
import { ApiTransactionsService } from 'src/api/services';
import { of } from 'rxjs';

@Injectable()
export class RootEffects {
    constructor(
        protected readonly actions$: Actions,
        private readonly router: Router,
        private readonly storageService: LocalStorageService,
        private readonly transactionsService: ApiTransactionsService
    ) { }

    private readonly initAction = [GetCurrentUser(), GetUserBalance()];

    loginSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LoginSuccess),
            tap(() => {
                this.router.navigateByUrl('/');
            }),
            switchMap(() => [InitializeApp()])
        )
    );

    getCurrentUser$ = createEffect(() =>
        this.actions$.pipe(
            ofType(GetCurrentUser),
            switchMap(() => {
                return []
            }
            )
        ));

    getUserBalance$ = createEffect(() =>
        this.actions$.pipe(
            ofType(GetUserBalance),
            switchMap(() => this.transactionsService.balance().pipe(
                map(balance => GetUserBalanceSuccess({ balance })),
                catchError(error => of(SomeError({ action: 'getUserBalance', error }))
                ))))
    );

    initializeApp$ = createEffect(() =>
        this.actions$.pipe(
            ofType(InitializeApp),
            switchMap(() => this.initAction)
        )
    );

    logout$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(Logout),
                tap(() => {
                    this.storageService.delete(DBkeys.JWT_TOKEN);
                    this.router.navigateByUrl('/account/login');
                })
            ),
        { dispatch: false }
    );

    someError$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(SomeError),
                tap((errorData) => console.error(errorData))
            ),
        { dispatch: false }
    );
}

