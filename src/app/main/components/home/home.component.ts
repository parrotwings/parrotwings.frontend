import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiTransactionsService, ApiTransactionRequest, ApiTableFilterModel } from 'src/api';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { IAppState, getUserBalance, InitializeApp } from 'src/app/store';
import { LazyLoadEvent } from 'primeng/api';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public transactionForm = new FormGroup({
    userEmail: new FormControl(null, Validators.required),
    amount: new FormControl(null, [Validators.required]),
  });

  public loading = false;
  public totalRecords = 0;
  public transaction: any[] = [];

  public searchCriteria: LazyLoadEvent = {
    first: 0,
    globalFilter: null,
    rows: 10,
    sortOrder: 1,
    filters: {}
  };

  public balance$: Observable<number>;

  constructor(private readonly store: Store<IAppState>, private readonly transactionsService: ApiTransactionsService) {
    this.store.dispatch(InitializeApp());

    this.balance$ = this.store.pipe(select(getUserBalance));
  }

  public ngOnInit(): void {
  }

  public load(criteria): void {
    if (criteria) {
      this.searchCriteria = criteria;
    }

    this.loading = true;

    const searchCriteria: ApiTableFilterModel = criteria;
    debugger;
    this.transactionsService.transactions({ body: searchCriteria }).subscribe(response => {
      this.transaction = response.transactions;
      this.totalRecords = response.totalCount;
      this.loading = false;

    },
      error => {
        this.loading = false;
      })
  }

  public createTransaction() {
    const { userEmail, amount } = this.transactionForm.value;

    const request: ApiTransactionRequest = {
      amount: +amount,
      userEmail
    };

    this.transactionsService.createTransaction({ body: request }).subscribe(r => {
    })
  }
}
