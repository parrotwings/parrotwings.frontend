import { Component, OnInit } from '@angular/core';
import { IAppState } from 'src/app/store';
import { Store } from '@ngrx/store';

import * as fromRoot from '../../store';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  constructor(private readonly store: Store<IAppState>,) { }

  ngOnInit(): void {
  }

  public logout() {
    this.store.dispatch(fromRoot.Logout());
  }
}
